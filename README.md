# My Calculator

The next project we will practice essential bases, to interact with the interface and logic for a simple project in kotlin

## Project download

If you want to download the repository, you must follow the git clone command followed by the clone by HTTPS command:

```
git clone https://gitlab.com/android-kotlin3/my-calculator.git
```

***

## Project Summary

For this project, I will interact a little with the interface and I will practice to identify the components with the view, it interacts more with Linear Layouts both vertically and horizontally.

* In addition to performing the basic operations of a calculator:
  - Sum
  - Subtraction
  - Multiplication
  - Division
  - Clear
  - Equal

It is also validated if the operations are negative to the result for, to redo another operation

***

## Solution to present:

Important detail is that the project was carried out with Kotlin for the development and management of the interface state

***
![image](markdown/1.png)
